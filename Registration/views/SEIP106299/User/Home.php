<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>PhoneBook_by-Islam</title>

    <!-- Bootstrap -->
    <link href="../../../css/bootstrap.min.css" rel="stylesheet">
    <!--my sytle link-->
    <link href="../../../css/style.css" rel="stylesheet" type="text/css"/>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
      <!--body field start-->
      <div class="container-fluid">
          <div class="container">
              <h2>
                  Registration Project<br/><small>ID:106299</small>
                  <a href='../../../index.php'><button class='btn btn-default'>Logout</button></a>
              </h2>
          </div>
      </div>
          <div class="container">
              <div class="section">                  
                    <div class="table-responsive">
            <?php
                    function __autoload($classname){
                         $file =str_replace("\\","/",$classname);  
                         include ("../../../".$file.'.php');
                    }

                  use src\BITM\SEIP106299\User\User;
                  $user = new User();
                  $user->view();                  
                  $_user = $user->view();
             ?>
         <table class="table">
         <thead>
                        <tr>
                            <th>SL</th>
                            <th>Name</th>
                            <th>Email</th>
                            <th>Action</th>
                        </tr>
         </thead>
         <tbody>                     
                        <?php foreach ($_user as $user): ?>
                        <tr class="default">
                            <td><?php echo $user['id']; ?></td>
                            <td><?php echo $user['username']; ?></td>
                            <td><?php echo $user['email']; ?></td>   
                            <td><a class='btn btn-danger' href='Delete.php?id=<?php echo $user['id'] ; ?>'>Delete</a></td>             
                        </tr>
                        <?php endforeach; ?>
         </tbody>           
         </table>
                      </table>
                    </div>
              </div>
          </div>
      <div class="container-fluid">
          <div class="container">
              <div class="header">
                  <div class='col-md-4'>
                      <br/>
                      <p id='left'>copyright&copy2016</p>
                  </div>
                  <div class='col-md-4'>
                      <br/><center><img src='../../../img/icon.png' alt='icon_file_name' height="100%" width='100%'></center>
                  </div>
                  <div class='col-md-4'>
                      <br/>
                      <p id='right'>Powered By-BITM(Basis)<br/>
                             Developed By- Islam Hossain</p>
                  </div>
              </div>
          </div>
      </div>
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
  </body>
</html>