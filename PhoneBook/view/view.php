<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Bootstrap 101 Template</title>

    <!-- Bootstrap -->
    <link href="../css/bootstrap.min.css" rel="stylesheet">
    <!--mystyle add-->
    <link href="../css/style.css" type="text/css" rel="stylesheet">

  </head>
  <body>
      <div class="container-fluid">
          <div class="container">
              <h2>
                  Registration Project<br/><small>ID:106299</small>
                  <a href='../index.php'><button class='btn btn-danger'>Back </button></a>
              </h2>
          </div>
      </div>
      <!--header end-->
      <div class="container">
          <div class="content">  
<section>
<div class="col-md-2"></div>
<div class="col-md-8">
<div id="myData">
    
<?php
function __autoload($className){
$filename = str_replace("\\","/",$className);
include_once ("../".$filename.".php");
}

use src\Phone;
$id=$_GET['id'];
$object=new Phone();
$phone= $object->view($id); 

         foreach($phone as $phno):
         ?>
    <table>
        <tr><th>ID:</th><td><?Php echo $phno['id'];?></td>
        <tr><th>Name:</th><td><?Php echo $phno['name'];?></td>
        <tr><th>Cell No:</th><td><?php  echo implode(", ",unserialize($phno['phoneno']));?></td>
        <tr><th>Address:</th><td><?Php echo $phno['address'];?></td>        
    </table>
        
        <?php 
        endforeach;
        ?>

    </div>
    </div>
</section>                     
      </div>
      </div>
      </div>
      <!--content end-->
      <div class="container-fluid">
          <div class="container">
              <div class="header">
                  <div class='col-md-4'>
                      <br/>
                      <p id='left'>copyright&copy2016</p>
                  </div>
                  <div class='col-md-4'>
                      <br/><center><img src='img/icon.png' alt='icon_file_name' height="100%" width='100%'></center>
                  </div>
                  <div class='col-md-4'>
                      <br/>
                      <p id='right'>Powered By-BITM(Basis)<br/>
                             Developed By- Islam Hossain</p>
                  </div>
              </div>
          </div>
      </div>
      <!--footer end-->

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
  </body>
</html>

