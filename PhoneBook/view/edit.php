<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Bootstrap 101 Template</title>

    <!-- Bootstrap -->
    <link href="../css/bootstrap.min.css" rel="stylesheet">
    <!--mystyle add-->
    <link href="../css/style.css" type="text/css" rel="stylesheet">

  </head>
  <body>
      <div class="container-fluid">
          <div class="container">
              <h2>
                  Registration Project<br/><small>ID:106299</small>
                  <a href='../index.php'><button class='btn btn-danger'>Back </button></a>
              </h2>
          </div>
      </div>
      <!--header end-->
      <div class="container">
          <div class="content">  
<section>
<div class="col-md-2"></div>
<div class="col-md-8">
<div id="myData">
 <?php
function __autoload($className){
    $filename = str_replace("\\","/",$className);
    include_once ("../".$filename.".php");
}

use src\Phone;
$id=$_GET['id'];
$obj=new Phone();
$phonebook=$obj->update($id);

//print_r($phonebook);
?>
 <form>
  <div class="form-group">
    <label for="exampleInputText1">Email address</label>
    <input type="text" name="name" value="<?php echo $phonebook[0]['name']; ?>" class="form-control" id="exampleInputEmail1" placeholder="Enter Name">
  </div>
  <div class="form-group">
            <?php
                $phone = unserialize($phonebook[0]['phoneno']);
                foreach ($phone as $phone_number): 
    { ?>
            <span id="btn2"><input type='text' name=phoneno[] maxlength=11 value="<?php echo $phone_number; ?>" class="form-control" id="exampleInputEmail1" placeholder='Enter Contact Number'></span><br/>
                <?php          
  }
               endforeach; 

            ?>
            <span id="btn1">Add More</span><br>
  </div>
 <div class="form-group">
          <label for="address">Address:</label>
          <textarea name="address" class="form-control" rows="5" id="comment" placeholder='Enter Your Address'><?php echo $phonebook[0]['address']; ?></textarea>
</div>

  <button type="submit" class="btn btn-danger">Submit</button>
</form>


    </div>
    </div>
</section>                     
      </div>
      </div>
      <!--content end-->
      <div class="container-fluid">
          <div class="container">
              <div class="header">
                  <div class='col-md-4'>
                      <br/>
                      <p id='left'>copyright&copy2016</p>
                  </div>
                  <div class='col-md-4'>
                      <br/><center><img src='img/icon.png' alt='icon_file_name' height="100%" width='100%'></center>
                  </div>
                  <div class='col-md-4'>
                      <br/>
                      <p id='right'>Powered By-BITM(Basis)<br/>
                             Developed By- Islam Hossain</p>
                  </div>
              </div>
          </div>
      </div>
      <!--footer end-->

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
    <script>
$(document).ready(function(){
    $("#btn1").click(function(){
        $("#btn2").append("<br><span><input type=text name=phoneno[] maxlength=11 class='form-control' id='exampleInputEmail1'></span>");
    });
});
</script>
  </body>
</html>
