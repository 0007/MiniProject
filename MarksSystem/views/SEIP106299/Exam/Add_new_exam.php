<?php

function __autoload($classname){
    //$file ="../../../src/BITM/SEIP106299/Mobile";
    //include ($file.$classname.'.php');
    $file =str_replace("\\","/",$classname);  
    include ("../../../".$file.'.php');
}

use src\BITM\SEIP106299\Admin\Batch;
$batch = new Batch();
$batch->Show_batch();
$_batch = $batch->Show_batch();


session_start();
    if(isset($_SESSION['user_email'])){       
    
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Trainee Maintenance System</title>

    <!-- Bootstrap -->
    <link href="../../../css/bootstrap.min.css" rel="stylesheet">
    <!-- My selected style -->
    <link href="../../../css/style.css" rel="stylesheet">

  </head>
  <body>
      <div class="container-fluid">
          <div class="container">
              <h1>
                  <small><img src="../../../image/icon.png" alt="Basis_LOGO" height="20%" width="10%"></small>
                  <header>Trainee Maintenance System</header>
                  <small id="logout_btn"><a class="btn btn-primary" href="logout.php">Logout</a></small>
              </h1>
          </div>          
      </div>
      <!-- header section end -->
      <div class="container">
          <div class="nav">
              <div class="col-md-2"></div>
              <div class="col-md-10">
              <nav>
                  <div class="col-md-2"></div>
                  <div class="col-md-2"><a class="" href="../Admin/Home.php">Home</a></div>
                  <div class="col-md-2"><a class="" href="../Admin/Add_batch.php">Add Batch</a></div>
                  <div class="col-md-2"><a class="" href="Show_exam_batch.php">Exam</a></div>
                  <div class="col-md-2"><a class="" href="">Result</a></div>
                  <div class="col-md-2"><a class="" href="">Tutorial</a></div>                  
              </nav>
              </div>
          </div>
          <div class="content">
<!--//////////////////////////--->              
<div class="col-md-3"></div>


<div class="col-md-6" style="background-color:#E5E4E2;">
    <form name="add_batch_form" onsubmit="return validate();" enctype="multipart/form-data"action="#" method="post">
                      <center><h4>Add New Exam</h4><br/><br/></center>
                      <div class="col-md-6">
                          <div class="form-group">
                            <label for="exampleInputText1">Select Batch No</label>
                             <select name="batch_no" class="form-control">
                              <option>.............</option>
                                <?php 
                                   foreach ($_batch as $batch): 
                                       echo "<option>".$batch['batch_no']."</option>"; 
                                   endforeach;
                                ?> 
                             </select>
                            </div>
                      </div>
                      <div class="col-md-6">
                    <div class="form-group">
                      <label for="exampleInputText1">Select Batch Name</label>
                      <select name="batch_name" class="form-control">
                          <option>................</option>
                               <?php 
                                   foreach ($_batch as $batch): 
                                       echo "<option>".$batch['batch_name']."</option>"; 
                                   endforeach;
                                ?>
                        </select>
                    </div>
                    </div>
                    <div class="form-group">
                      <label for="exampleInputText1">Exam No</label>
                      <input type="text" name="exam_no" class="form-control" id="exampleInputEmail1" placeholder="Enter Exam No">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputText1">Exam Name</sup style='color:red;'>*</sup></label>
                      <input type="text" name="exam_name" class="form-control" id="exampleInputEmail1" placeholder="Enter Exam Name">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputText1">Exam Date</sup style='color:red;'>*</sup></label>
                        <input type="date" name="exam_date" class="form-control" id="exampleInputEmail1" placeholder="dd-mm-yyyy">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputText1">Exam Question</sup style='color:red;'>*</sup></label>
                        <input type="file" name="exam_question" class="form-control" id="exampleInputEmail1">
                    </div>
                    <button type="submit" class="btn btn-primary">Save New Batch</button>
                 </form>
    <br/>
</div>

<!--//////////////////////////--->              
          </div><!--content close-->  
      </div>
      <!-- container section end -->
      <div class="container-fluid">
          <div class="container">
              <div class="col-md-4"><br/>Developed By-<a href="">Islam Hossain</a></div>
              <div class="col-md-4"><br/>copyright &copy 2016</div>
              <div class="col-md-4"><br/>Powered By- Basis</div>
          </div> 
      </div>
      <!-- footert section end -->
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
</body>
</html>
<?php
   
    }else{
        echo "<script>alert('You Are Not A Valid User !');</script>";
        echo "<script>window.location='../../../index.php';</script>";
    }
    
?>