-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jan 30, 2016 at 04:57 AM
-- Server version: 5.6.21
-- PHP Version: 5.6.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `markssystem_by_islam_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `markssystem_by_islam_trainee_tb`
--

CREATE TABLE IF NOT EXISTS `markssystem_by_islam_trainee_tb` (
`id` int(11) NOT NULL,
  `batch_no` varchar(255) NOT NULL,
  `batch_name` varchar(255) NOT NULL,
  `seid_no` varchar(255) NOT NULL,
  `trainee_name` varchar(255) NOT NULL,
  `trainee_email` varchar(255) NOT NULL,
  `trainee_phone` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `markssystem_by_islam_trainee_tb`
--

INSERT INTO `markssystem_by_islam_trainee_tb` (`id`, `batch_no`, `batch_name`, `seid_no`, `trainee_name`, `trainee_email`, `trainee_phone`) VALUES
(22, '1st', 'Web Design', '101', 'Islam Hossain', 'islam20162016@gmail.com', '+8801914441334'),
(23, '1st', 'Web Development- PHP', '108298', 'Shirin Tumpa', 'islamislam@gmail.com', '+8801700014712'),
(24, '1st', 'SEO', '102300', 'Islam Hossain', 'islamhoss@gmail.com', '01478956'),
(25, '2nd', 'seo', '123456', 'namesomethin', 'name@g.com', '4546987'),
(26, '1st', 'Web Development- PHP', '100052', 'Hasan Masud', 'hasan@gm.com', '45698712365'),
(27, '1st', 'Web Development- PHP', '102301', 'Islam Hossain', 'islamhoss@gmail.com', '45698712365');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `markssystem_by_islam_trainee_tb`
--
ALTER TABLE `markssystem_by_islam_trainee_tb`
 ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `markssystem_by_islam_trainee_tb`
--
ALTER TABLE `markssystem_by_islam_trainee_tb`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=28;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
